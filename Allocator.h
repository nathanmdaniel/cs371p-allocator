// ----------------------------------
// projects/c++/allocator/Allocator.h
// Copyright (C) 2019
// Glenn P. Downing
// ----------------------------------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Goes through char[] a verify that that sentinels on either side of
     *   a chunk match up, that the number of bytes between is accurate, and
     *   that the total number of bytes consumed is always equal to N
     */
    bool valid() const {
        // <your code>
        bool valid = true;
        int total = 0;
        char* it = (char*)a;
        char* end = (char*)(a + N);
        while (valid && it < end) {
            int alloc = abs(*(int*)it);
            total += (alloc + 8);
            valid &= (alloc == abs(*(int*)(it + alloc + 4)));
            it += (alloc + 8);
        }
        valid &= (total == N);
        return valid;
    }

public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator(int* p) :
            _p(p)
        {}

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // <your code>
            return *_p;
        }

        // ----------
        // operator * (MINE)
        // ----------
        int& operator * () {
            return *_p;
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            // <your code>
            char* temp = (char*)_p;
            // moves length of chunk given by first sentinel
            temp += abs(*_p);
            _p = (int*)temp;
            // moves distance of 2 sentinels
            _p += 2;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x((*this)._p);
            ++*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator() {
        // <your code>
        if (N < sizeof(T) + (2 * sizeof(int))) {
            throw std::bad_alloc();
        }
        // beginning 4 bytes, then all inside available, then end 4 bytes
        (*this)[0] = N - 8;
        // make end marker show size of available chunk
        (*this)[N - 4] = N - 8;

        assert(valid());
    }

    my_allocator(const my_allocator&) = default;
    ~my_allocator() = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------

    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate(size_type n) {
        // <your code>

        int size = abs((int)n) * sizeof(T) + 8;
        iterator it((int*)a);
        iterator end((int*)(a + N));

        // finds chunk large enough to allocate from
        while (*it < (size - 8)) {
            if (it == end) {
                throw std::bad_alloc();
            }
            ++it;
        }

        pointer ret = (T*)((char*)&(*it) + 4);

        // size of chunk before splitting
        const int former = *it;

        // new chunk
        int& alloc = *it;
        // checks if after split, there would be enough room for another allocation
        bool split = (alloc - size) >= (int)sizeof(T);

        if (split) {
            alloc = -1 * (size - 8);
            // set end sentinel of new chunk
            *(int*)((char*)&(*it) + (size - 4)) = alloc;


            // old chunk now with new allocation removed
            ++it;
            int& chunk = *it;
            chunk = former - (size);

            // set end sentinel of old chunk
            *(int*)((char*)&(*it) + chunk + 4) = chunk;
        }
        else {
            // allocate entire chunk, negate sentinels
            *(int*)((char*)&(*it) + (alloc + 4)) *= -1;
            alloc *= -1;
        }

        assert(valid());


        return ret;
    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct(pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     */
    void deallocate(pointer p, size_type) {
        // <your code>
        char* charp = (char*)p;

        if (charp < (char*)a || charp >(char*)(a + N))
            throw std::invalid_argument("Param p out of bounds.");

        // make chunk's sentinels positive again
        int& psize = *((int*)(charp - 4));
        psize *= -1;
        *(int*)(charp + psize) = psize;


        char* right = charp + abs(*((int*)(charp - 4))) + 8;
        if (right < (a + N)) {
            const int& rval = *((int*)(right - 4));
            if (rval > 0) {
                // coalesce
                *(int*)(charp - 4) += (rval + 8);
                *(int*)(charp + *(int*)(charp - 4)) = *(int*)(charp - 4);
            }
        }

        char* left = charp - abs(*((int*)(charp - 8))) - 8;
        if (left > a) {
            const int& lval = *((int*)(left - 4));
            if (lval > 0) {
                // coalesce
                *(int*)(left - 4) += (psize + 8);
                *(int*)(left + *(int*)(left - 4)) = *(int*)(left - 4);
            }
        }

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }
};

// For Allocator.c++
int tests_read(std::istream& r);
void allocate_solve (std::istream& r, std::ostream& w);

#endif // Allocator_h
