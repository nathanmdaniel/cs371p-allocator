// ------------------------
// Allocator
// ------------------------



// ------------
// includes
// ------------

#include <iostream>
#include <algorithm>
#include <cassert>  // assert
#include <sstream>  // istringstream
#include <string>   // getline, string
#include "Allocator.h"

using namespace std;

// ------------
// tests_read
// ------------

int tests_read(istream& r) {
    int tests;
    string name;
    r >> tests;
    // move to line containing first test
    getline(r, name); // string()
    getline(r, name);
    assert(tests > 0);
    return tests;
}



// ------------
// allocate_solve
// ------------

void allocate_solve (istream& r, ostream& w) {
    int tests = tests_read(r);
    while (tests) {
        my_allocator<double, 1000> allocator;
        string line;
        getline(r, line);
        double* begin = NULL;

        while (line != "") {
            istringstream sin(line);
            int request;
            sin >> request;
            if (request > 0) {
                double* temp = allocator.allocate(request);
                if (!begin)
                    begin = temp;
            }
            else if(request < 0) {
                my_allocator<double, 1000>::iterator it((int*)begin - 1);
                while (*it > 0)
                    ++it;
                while (request < -1) {
                    ++it;
                    while (*it > 0)
                        ++it;
                    ++request;
                }
                allocator.deallocate((double*)((&(*it)) + 1), *it / 8.0);
            }
            line = "";
            getline(r, line);
        }

        char* end = ((char*)begin) + 996;
        my_allocator<double, 1000>::iterator bit((int*)begin - 1);
        my_allocator<double, 1000>::iterator eit((int*)end);
        while (bit != eit) {
            w << *bit;
            ++bit;
            if (bit != eit)
                w << " ";
            else
                w << "\n";
        }
        --tests;
    }

}