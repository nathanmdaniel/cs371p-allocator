.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

FILES :=                                  \
	RunAllocator.c++						\
	Allocator.c++						\
    Allocator.h                             \
    makefile                              \
    TestAllocator.c++							\
    RunAllocator.in                         \
    RunAllocator.out                        \
    .gitignore                            \
    Allocator.log                           \
    html                                  \

# uncomment these four lines when you've created those files
# you must replace GitLabID with your GitLabID
#    voting-tests/GitLabID-RunVoting.in  \
#    voting-tests/GitLabID-RunVoting.out \

allocator-tests:
	git clone https://gitlab.com/gpdowning/cs371p-allocator-tests.git allocator-tests

html: Doxyfile Allocator.h
	doxygen Doxyfile

Allocator.log:
	git log > Allocator.log

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATEIC to YES
Doxyfile:
	doxygen -g

RunAllocator: Allocator.h RunAllocator.c++ Allocator.c++
	-cppcheck RunAllocator.c++
	-cppcheck Allocator.c++
	g++ -pedantic -std=c++14 -Wall -Weffc++ -Wextra Allocator.h Allocator.c++ RunAllocator.c++ -o RunAllocator

RunAllocator.c++x: RunAllocator
	./RunAllocator < RunAllocator.in > RunAllocator.tmp
	-diff RunAllocator.tmp RunAllocator.out

TestAllocator: Allocator.h TestAllocator.c++
	-cppcheck Allocator.h
	-cppcheck TestAllocator.c++
	g++ -fprofile-arcs -ftest-coverage -pedantic -std=c++14 -Wall -Weffc++ -Wextra  Allocator.h TestAllocator.c++ -o TestAllocator -lgtest -lgtest_main -pthread

TestAllocator.c++x: TestAllocator
	valgrind ./TestAllocator
	gcov -b TestAllocator.c++ | grep -A 5 "File '.*Allocator.h'"

all: RunAllocator TestAllocator

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunAllocator
	rm -f TestAllocator

config:
	git config -l

ctd:
	checktestdata TestAllocator.ctd RunAllocator.in

docker:
	docker run -it -v $(PWD):/usr/voting -w /usrvoting gpdowning/gcc

format:
	astyle Allocator.h
	astyle Allocator.c++
	astyle RunAllocator.c++
	astyle TestAllocator.c++

#init:
#	touch README
#	git init
#	git remote add origin git@gitlab.com:gpdowning/cs371p-collatz.git
#	git add README
#	git commit -m 'first commit'
#	git push -u origin master

#pull:
#	make clean
#	@echo
#	git pull
#	git status

#push:
#	make clean
#	@echo
#	git add .gitignore
#	git add .gitlab-ci.yml
#	git add Collatz.c++
#	git add Collatz.h
#	-git add Collatz.log
#	-git add html
#	git add makefile
#	git add RunCollatz.c++
#	git add RunCollatz.in
#	git add RunCollatz.out
#	git add Test.ctd
#	git add TestCollatz.c++
#	git commit -m "another commit"
#	git push
#	git status

run: RunAllocator.c++x TestAllocator.c++x
#run: TestAllocator.c++x

#scrub:
#	make clean
#	rm -f  Collatz.log
#	rm -f  Doxyfile
#	rm -rf collatz-tests
#	rm -rf html
#	rm -rf latex

#status:
#	make clean
#	@echo
#	git branch
#	git remote -v
#	git status

versions:
	which         astyle
	astyle        --version
	@echo
	dpkg -s       libboost-dev | grep 'Version'
	@echo
	ls -al        /usr/lib/*.a
	@echo
	which         checktestdata
	checktestdata --version
	@echo
	which         cmake
	cmake         --version
	@echo
	which         cppcheck
	cppcheck      --version
	@echo
	which         doxygen
	doxygen       --version
	@echo
	which         g++
	g++           --version
	@echo
	which         gcov
	gcov          --version
	@echo
	which         git
	git           --version
	@echo
	which         make
	make          --version
	@echo
	which         valgrind
	valgrind      --version
	@echo
	which         vim
	vim           --version
