// ------------------------
// RunAllocator
// ------------------------



// ------------
// includes
// ------------

#include <iostream>
#include <algorithm>
#include <cassert>  // assert
#include <sstream>  // istringstream
#include "Allocator.h"



int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */

    allocate_solve(std::cin, std::cout);
    return 0;
}
